/*
1. Create a package.json for our application
	npm init 
2. Create a file called index.js
3. Install express
	npm install express
4. Create an express app (lines 12 and 14)
5. Create a GET route "/home" that will print out a simple "This is home" message.
6. Listen to the port.
7. Make a mock database called user (let users = [])
8. Get route that will access "/users" that will retrieve all the users in the mock database. res.send (users)
9. Create a route to add users
	-last name
	-first name
	-email
*/


const express = require ('express');

const app = express ();

const port = 3000;

let users = []

//middleware
app.use(express.json()); 
app.use(express.urlencoded({extended:true}));


//GET route
app.get('/home', (req, res) => {
	res.send('This is home.')
})

//GET route
app.get('/users', (req, res) => {
	res.send(users)
})

//To add users
app.post ('/signup', (req, res) => {
	console.log(req.body)
	if (req.body.username !=='' && req.body.password !=='' && req.body.email !==''){
		users.push(req.body)
		res.send (`User ${req.body.username} successfully registered!`)
	}
	else {
		res.send (`Please input username, password, and email.`)
	}
})


app.listen(port, () => console.log(`Server running at port ${port}`))
